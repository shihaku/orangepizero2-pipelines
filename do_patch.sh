#!/bin/bash

find patches -mindepth 1 -type d \
	-exec bash -c 'p={}; target=$(basename ${p}); patch -p 1 -N -d ${WORKSPACE}/$target < ${p}/*' \;
