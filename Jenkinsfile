pipeline {
  agent { node { label 'ubuntu' } }
  environment {
    BUILDDIR = "${env.WORKSPACE}/build"
    PATH = "${env.WORKSPACE}/poky/scripts:${env.WORKSPACE}/poky/bitbake/bin:${env.PATH}"
  }

  parameters {
    string(name: 'packages', defaultValue: '', description: 'Enter packages to rebuild')
  }

  stages {
    stage('checkout') {
      steps {
        checkout([$class: 'GitSCM', branches: [[name: '*/kirkstone']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'poky']], submoduleCfg: [], userRemoteConfigs: [[url: 'git://git.yoctoproject.org/poky']]])
        checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'meta-allwinner']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'shihaku-bitbucket', url: 'https://shihaku@bitbucket.org/shihaku/meta-allwinner.git']]])
        checkout([$class: 'GitSCM', branches: [[name: '*/h616-v9-orangepi-zero2']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'CheckoutOption', timeout: 20],[$class: 'RelativeTargetDirectory', relativeTargetDir: 'linux-mainline']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'shihaku-bitbucket', url: 'https://shihaku@bitbucket.org/shihaku/linux-mainline.git']]])
      }
    }
    stage('prepare') {
      steps {
        script {
          sh """#!/bin/bash
            rm -rf ${env.BUILDDIR}/conf
            source ${env.WORKSPACE}/poky/oe-init-build-env ${env.BUILDDIR}

            bitbake-layers layerindex-fetch meta-oe
            bitbake-layers add-layer ${env.WORKSPACE}/meta-allwinner

            cp ${env.WORKSPACE}/local.conf ${env.BUILDDIR}/conf/local.conf
            cat >>${env.BUILDDIR}/conf/local.conf<<EOF
EXTERNALSRC:pn-linux-mainline = "${env.WORKSPACE}/linux-mainline"
EOF
          """
        }
      }
    }
    stage('patch') {
      steps {
        script {
          sh """#!/bin/bash
            ./do_patch.sh
          """
        }
      }
    }
    stage('rebuild packages') {
      steps {
        script {
          if ("${params.packages}" == '')
            return

          def package_list = "${params.packages}".split(',')
          for (package_name in package_list) {
            println package_name
            sh """#!/bin/bash
              source ${env.WORKSPACE}/poky/oe-init-build-env ${env.BUILDDIR}

              bitbake "${package_name}" -c cleanall
              bitbake "${package_name}"
            """
          }
        }
      }
    }
    stage('build image') {
      steps {
        script {
          sh """#!/bin/bash
            source ${env.WORKSPACE}/poky/oe-init-build-env ${env.BUILDDIR}

            bitbake core-image-minimal -c cleanall
            bitbake core-image-minimal
          """
        }
      }
    }
    stage('populate sdk')  {
      steps {
        script {
          sh """#!/bin/bash
            source ${env.WORKSPACE}/poky/oe-init-build-env ${env.BUILDDIR}

            bitbake core-image-minimal -c populate_sdk
          """
        }
      }
    }
  }
}
